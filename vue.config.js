const { defineConfig } = require('@vue/cli-service')
const Dotenv = require('dotenv-webpack')
const path = require('path')
const vueSrc = './src'
const vueBusiness = './src/business'

module.exports = defineConfig({
  transpileDependencies: true,
  runtimeCompiler: true,
  configureWebpack: {
    resolve: {
      alias: {
        '@': path.resolve(__dirname, vueSrc),
        '@b': path.resolve(__dirname, vueBusiness)
      },
      extensions: ['.js', '.vue', '.json']
    },
    plugins: [new Dotenv()]
  }
})
