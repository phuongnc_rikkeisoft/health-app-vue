import MealItemComponent from '@/components/Meals/MealItemComponent'
import ButtonTopPageComponent from '@/components/Button/ButtonTopPageComponent'
import { mapState } from 'vuex'
import axios from 'axios'
import api from '@/constants/api'

export default {
  name: 'MealList',

  data() {
    return {
      url: process.env.BASE_API_URL + api.GET_MEAL_LIST
    }
  },

  components: {
    MealItemComponent,
    ButtonTopPageComponent
  },

  computed: {
    ...mapState({
      mealsList: (state) => state.meal.mealsList
    })
  },

  async created() {
    const { data } = await axios.get(this.url)
    this.$store.commit('meal/SET_MEAL_DATA', data)
  }
}
