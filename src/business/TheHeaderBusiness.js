import NavBar from '@/components/Navigation/NavBarComponent'
import logo from '@/assets/icons/logo.svg'
import { mapState } from 'vuex'

const resizeEvent = 'resize'

export default {
  name: 'Header',

  components: {
    NavBar
  },

  data() {
    return {
      logoImg: logo,
      configImg: {
        width: 110,
        height: 40
      },
      screenWidth: window.innerWidth,
      isHiddenNav: false
    }
  },

  computed: {
    ...mapState({
      navItems: (state) => state.navItems || [],
      menuItems: (state) => state.menuItems || []
    }),

    menuItemsSMP() {
      if (this.isHiddenNav) {
        return this.menuItems.concat(this.navItems)
      }

      return this.menuItems
    },

    navItemsPC() {
      if (this.isHiddenNav) {
        return []
      }

      return this.navItems
    }
  },

  created() {
    window.addEventListener(resizeEvent, this.handleResizeBrowser)
    this.isHiddenNav = window.innerWidth < 840
  },

  methods: {
    handleResizeBrowser() {
      this.screenWidth = window.innerWidth
      this.isHiddenNav = window.innerWidth < 840
    }
  },

  destroyed() {
    window.removeEventListener(resizeEvent, this.handleResizeBrowser)
  }
}
