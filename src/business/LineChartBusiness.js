import { Chart } from 'chart.js'

export default {
  name: 'LineChartComponent',

  data() {
    return {
      configOptions: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        scales: {
          xAxes: [
            {
              gridLines: {
                drawTicks: false,
                zeroLineWidth: 1,
                zeroLineColor: '#777777',
                color: '#777777',
                lineWidth: 1
              },
              ticks: {
                fontColor: '#d8d8d8',
                fontSize: 14
              }
            }
          ],
          yAxes: [
            {
              gridLines: {
                lineWidth: 0,
                zeroLineWidth: 3,
                zeroLineColor: '#2e2e2e'
              },
              ticks: {
                beginAtZero: true,
                max: 5.5,
                fontColor: '#2e2e2e',
                fontSize: 14
              }
            }
          ]
        }
      }
    }
  },

  computed: {
    chartData() {
      return {
        labels: [
          '6月',
          '7月',
          '8月',
          '9月',
          '10月',
          '11月',
          '12月',
          '1月',
          '2月',
          '3月',
          '4月',
          '5月'
        ],
        datasets: [
          {
            label: 'data1',
            data: [5, 4.8, 3.4, 3.7, 3.6, 3, 3.5, 2.8, 2.7, 2.6, 2.3, 2.6],
            fill: false,
            borderColor: '#ffcc21',
            tension: 0,
            radius: 4
          },
          {
            label: 'data2',
            data: [5, 4.6, 3.6, 3.3, 2.7, 2.65, 2.3, 2.2, 2.1, 1.3, 1.1, 0.9],
            fill: false,
            borderColor: '#8fe9d0',
            tension: 0,
            radius: 4
          }
        ]
      }
    }
  },

  mounted() {
    const ctx = document.getElementById('line-chart').getContext('2d')
    const config = {
      type: 'line',
      data: this.chartData,
      options: this.configOptions
    }
    new Chart(ctx, config)
  }
}
