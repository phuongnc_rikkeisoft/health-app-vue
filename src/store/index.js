import Vue from 'vue'
import Vuex from 'vuex'
import meal from '@/store/meal/index'
import myRecord from '@/assets/icons/my-record.svg'
import challenge from '@/assets/icons/challenge.svg'
import notice from '@/assets/icons/notice.svg'
import mealsOfTheDay from '@/assets/icons/meals-of-the-day.svg'
import snack from '@/assets/icons/snack.svg'

Vue.use(Vuex)

const navItems = [
  {
    icon: myRecord,
    name: 'my-record',
    title: '自分の記録'
  },
  {
    icon: challenge,
    name: 'challenge',
    title: 'チャレンジ'
  },
  {
    icon: notice,
    name: 'news',
    title: 'お知らせ'
  }
]
const menuItems = [
  {
    icon: '',
    name: 'my-record',
    title: '自分の記録'
  },
  {
    icon: '',
    name: '/a',
    title: '体重グラフ'
  },
  {
    icon: '',
    name: '/b',
    title: '目標'
  },
  {
    icon: '',
    name: '/c',
    title: '選択中のコース'
  },
  {
    icon: '',
    name: '/d',
    title: 'コラム一覧'
  },
  {
    icon: '',
    name: '/e',
    title: '設定'
  }
]
const mealsOfTheDayItems = [
  {
    icon: mealsOfTheDay,
    title: 'Morning'
  },
  {
    icon: mealsOfTheDay,
    title: 'Lunch'
  },
  {
    icon: mealsOfTheDay,
    title: 'Dinner'
  },
  {
    icon: snack,
    title: 'Snack'
  }
]
const navItemsFooter = [
  '会員登録',
  '運営会社',
  '利用規約',
  '個人情報の取扱について',
  '特定商取引法に基づく表記',
  'お問い合わせ'
]

export default new Vuex.Store({
  state: {
    navItems: navItems,
    menuItems: menuItems,
    mealsOfTheDayItems: mealsOfTheDayItems,
    navItemsFooter: navItemsFooter
  },
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    meal
  }
})
