export const SET_MEAL_DATA = 'SET_MEAL_DATA'
export const APPEND_MEAL_LIST = 'APPEND_MEAL_LIST'

export default {
  SET_MEAL_DATA,
  APPEND_MEAL_LIST
}
