import * as types from './mutation-types'

export default {
  [types.SET_MEAL_DATA](state, data) {
    state.mealsList = data
  },
  [types.APPEND_MEAL_LIST](state, data) {
    state.mealsList = [...state.mealsList, ...data]
  }
}
